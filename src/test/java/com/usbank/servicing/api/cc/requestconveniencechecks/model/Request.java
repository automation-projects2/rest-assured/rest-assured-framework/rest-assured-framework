package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import lombok.Data;

@Data
public class Request {
	private IdentifierType identifierType;
	private String accountToken;
	private AccountIdentifier accountIdentifier;


	public String positiveUID() {
		return "{\"request\":{\"identifierType\":\"" + identifierType + "\"," + accountIdentifier.positiveUID() + "}}";
	}

	public String nullAccountToken() {
		return identifierType + "\"accountToken\":[" + accountToken + "]";
	}

}
