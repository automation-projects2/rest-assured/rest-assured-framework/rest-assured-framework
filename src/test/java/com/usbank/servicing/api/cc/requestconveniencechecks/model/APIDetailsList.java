package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class APIDetailsList {
	
	@JsonProperty("apiDetails")
	private List<APIDetails> apiDetails;

}
