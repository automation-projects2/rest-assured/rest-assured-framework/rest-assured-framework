package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;

@Getter
@lombok.NoArgsConstructor
public class PartialError {
	private String errorCode = StringUtils.EMPTY;
	private String errorDescription = StringUtils.EMPTY;
	private String errorKeyAttribute = StringUtils.EMPTY;
	private String apiId = StringUtils.EMPTY;

	public PartialError(String errorCode, String errorDescription, String errorKey, String apiId) {
		this.errorCode = Objects.nonNull(errorCode) ? errorCode : StringUtils.EMPTY;
		this.errorDescription = Objects.nonNull(errorDescription) ? errorDescription : StringUtils.EMPTY;
		errorKeyAttribute = Objects.nonNull(errorKey) ? errorKey : StringUtils.EMPTY;
		this.apiId = Objects.nonNull(apiId) ? apiId : StringUtils.EMPTY;

	}

}
