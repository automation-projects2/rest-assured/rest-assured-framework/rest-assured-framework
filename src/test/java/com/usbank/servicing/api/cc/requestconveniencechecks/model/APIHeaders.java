package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class APIHeaders {

	@JsonProperty("headerName")
	private String headerName;

	@JsonProperty("headerValue")
	private String headerValue;

}
