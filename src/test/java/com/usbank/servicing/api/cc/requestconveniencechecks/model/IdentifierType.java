package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import java.util.Objects;

public enum IdentifierType {
	ACCOUNT_IDENTIFIER("ACCOUNT_IDENTIFIER"), ACCOUNTTOKEN("ACCOUNTTOKEN");

	private String identifierType;

	public String getIdentifierType() {
		return identifierType;
	}

	public void setIdentifierType(String identifierType) {
		this.identifierType = identifierType;
	}

	private IdentifierType(String identifierType) {
		this.identifierType = identifierType;
	}

	public static boolean isValid(String value) {
		if (Objects.nonNull(value)) {
			for (IdentifierType identifierType : IdentifierType.values()) {
				if (identifierType.toString().equals(value)) {
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}

	public static boolean isValidEnumValue(IdentifierType value) {
		if (Objects.nonNull(value)) {
			for (IdentifierType identifierType : IdentifierType.values()) {
				if (identifierType.equals(value)) {
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;
	}
}
