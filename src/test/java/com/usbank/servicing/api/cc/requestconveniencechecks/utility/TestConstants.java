package com.usbank.servicing.api.cc.requestconveniencechecks.utility;

public class TestConstants {

	public TestConstants() {
	}

	public static final String CUSTOMER_DETAILS = "/customerIdTestData.json";

	public static final String NULL_ACCOUNT_TOKEN = "nullAccountToken.json";

	public static final String INVALID_ACCOUNT_TOKEN = "invalidAccountToken.json";

	public static final String EMPTY_ACCOUNT_TOKEN = "emptyAccountToken.json";

	public static final String BLANK_ACCOUNT_TOKEN = "blankAccountToken.json";

	public static final String NULL_UNIQUE_IDENTIFER = "nullUniqueIdentifer.json";

	public static final String INVALID_UNIQUE_IDENTIFERN = "invalidUniqueIdentifer.json";

	public static final String EMPTY_UNIQUE_IDENTIFER = "emptyUniqueIdentifer.json";

	public static final String BLANK_UNIQUE_IDENTIFER = "blankUniqueIdentifer.json";

	public static final String NULL_IDENTIFER_TYPE = "nullIdentifer.json";

}
