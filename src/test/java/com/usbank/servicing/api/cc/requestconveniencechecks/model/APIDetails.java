package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class APIDetails {
	
	@JsonProperty("apiType")
	private String apiType;

	@JsonProperty("url")
	private String url;
	
	@JsonProperty("param")
	private String param;
	
	@JsonProperty("method")
	private String method;
	
	@JsonProperty("apiHeaders")
	private List<APIHeaders> apiHeaders;

}
