package com.usbank.servicing.api.cc.requestconveniencechecks.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.google.gson.Gson;

import com.omnimobile.DataObjects.PropertyConfigs;
import com.omnimobile.core.SessionIDGenerator;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.APIDetails;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.APIDetailsList;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.APIHeaders;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.ConvenienceChecksDomainResponse;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.RewardDetailsICSResponse ;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.response.Response;
import io.restassured.http.Header;
import io.restassured.http.Headers;

public class TestUtility {

	private static PropertyConfigs propertyConfigs = new PropertyConfigs();
	public static final String TEST_DATA_PATH = "src/test/resources/data/";
	public static String ENVIRONMENT = System.getProperty("runenvironment") == null ? propertyConfigs.run_env
			: System.getProperty("runenvironment");
	public static String env = propertyConfigs.urlProperty.getProperty("environment");
	public static String user = propertyConfigs.urlProperty.getProperty("username");
	public static String password = propertyConfigs.urlProperty.getProperty("password");


	public TestUtility() {

	}

	/**
	 *
	 * @return list of customers in the environment specified
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public static org.json.JSONObject getCustomerIdList() throws FileNotFoundException, IOException, ParseException {
		org.json.JSONObject customerIds = new org.json.JSONObject();
		new JSONArray();
		org.json.JSONArray envData;
		JSONParser parser = new JSONParser();
		try {

			Object obj = parser.parse(new FileReader(TestConstants.TEST_DATA_PATH + TestConstants.CUSTOMER_DETAILS));
			org.json.JSONObject jsonObj = new org.json.JSONObject(obj.toString());
			envData = jsonObj.getJSONArray(env.toUpperCase());
			customerIds = (org.json.JSONObject) envData.get(0);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return customerIds;
	}

	public static org.json.JSONArray getCustomerIdListArray()
			throws FileNotFoundException, IOException, ParseException {
		// org.json.JSONObject customerIds = new org.json.JSONObject();
		new JSONArray();
		org.json.JSONArray envData = null;
		JSONParser parser = new JSONParser();
		try {

			Object obj = parser.parse(new FileReader(TestConstants.TEST_DATA_PATH + TestConstants.CUSTOMER_DETAILS));
			org.json.JSONObject jsonObj = new org.json.JSONObject(obj.toString());
			envData = jsonObj.getJSONArray(env.toUpperCase());
			// customerIds = (org.json.JSONObject) envData.get(0);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return envData;
	}

	/**
	 *
	 * @param fileName
	 * @return the json request object which has the account token
	 */
	public static org.json.JSONObject getConvenienceChecksRequest(String fileName, String folder) {
		org.json.JSONObject request = new org.json.JSONObject();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(TestConstants.REQUESTS_FOLDER_NAME + folder + fileName));
			request = new org.json.JSONObject(obj.toString());
		} catch (IOException | org.json.simple.parser.ParseException | JSONException e) {
			e.printStackTrace();
		}
		return request;
	}

	/**
	 *
	 //	 * @param fileName
	 * @return the json API details
	 */
	public static org.json.JSONObject getConvenienceChecksAPI(String folder) {
		org.json.JSONObject request = new org.json.JSONObject();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(folder));
			request = new org.json.JSONObject(obj.toString());
		} catch (IOException | org.json.simple.parser.ParseException | JSONException e) {
			e.printStackTrace();
		}
		return request;
	}

	public static String fetchAccountsTokenUsingFANSAPI(String customerId, String uniqueIdentifer)
			throws Exception {
		String sessionID = SessionIDGenerator.sessionId.toString().trim().replace("-", "");

		Response fansTokenResponse = fansAPiResponse(sessionID, customerId, uniqueIdentifer);
		String accountToken = fansTokenResponse.getBody().jsonPath().get(("tokens[0].accountToken")).toString();

		return accountToken;
	}

	public static Response generateConvenienceChecksApiResponseGraphql(String customerId, String sessionID, String requestBody)
			throws Exception {
		Response dataApiResponse = RestAssuredHttpClient.fetchConvenienceChecksResponse(customerId, sessionID, requestBody);
		return dataApiResponse;
	}

	/**
	 *
	 * @param customerId
	//	 * @param channelID
	//	 * @param applicationId
	 * @param request
	 * @return the response based on the request passed to reward details domain
	 * @throws Exception
	 */
	public static Response generateConvenienceChecksApiResponseGraphqlHeaders(String customerId, String request,
																			  String type) throws Exception {
		String sessionID = SessionIDGenerator.sessionId.toString().trim().replace("-", "");
		Response dataApiResponse = null;
		APIDetails apiDetails = getAPIDetails("ConvenienceChecks");
		// Headers headers = getAPIHeaders(apiDetails, sessionID);
		Headers headers = removeAPIHeaders(apiDetails, sessionID, type);

		String apiURL = apiDetails.getUrl().replaceAll("ENV", env) + apiDetails.getParam();
		System.out.println("Calling API URL: " + apiURL);
		System.out.println("request:" + request);
		dataApiResponse = RestAssured.given()
				.config(RestAssured.config().encoderConfig(
						EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))
				.auth().preemptive().basic(user, password).headers(headers).body(request).when().post(apiURL).then()
				.log().all().extract().response();
		return dataApiResponse;
	}

	public static APIDetails getAPIDetails(String apiType) {
		JSONObject jsonResponse = TestUtility.getConvenienceChecksAPI(TestConstants.API_DETAILS);
		Gson gson = new Gson();
		String jsonUserIdentifier = "";
		try {
			jsonUserIdentifier = jsonResponse.getString("apiDetailsList");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		APIDetailsList apiDetails = gson.fromJson(jsonUserIdentifier, APIDetailsList.class);

		return apiDetails.getApiDetails().stream().filter(a -> a.getApiType().equals(apiType)).findAny().get();
	}

	public static Headers getAPIHeaders(APIDetails apiDetails, String sessionId) {

		List<APIHeaders> apiHeaders = apiDetails.getApiHeaders();
		List<Header> headerlist = new ArrayList<>();
		apiHeaders.stream().forEach(a -> headerlist
				.add(new Header(a.getHeaderName(), a.getHeaderValue().replaceAll("SESSIONXID", sessionId))));
		Headers headers = new Headers(headerlist);
		return headers;
	}

	public static Headers removeAPIHeaders(APIDetails apiDetails, String sessionId, String headerType) {

		List<APIHeaders> apiHeaders = apiDetails.getApiHeaders();
		List<Header> headerlist = new ArrayList<>();
		apiHeaders.stream().filter(b -> !b.getHeaderName().equalsIgnoreCase(headerType)).forEach(a -> headerlist
				.add(new Header(a.getHeaderName(), a.getHeaderValue().replaceAll("SESSIONXID", sessionId))));
		Headers headers = new Headers(headerlist);
		System.out.println(headers);
		return headers;
	}

	/**
	 *
	 * @param customerId
	//	 * @param sessionID
	 * @return ICS call response
	 * @throws Exception
	 */
	public static Response generateRewardDetailsICSResponseGraphql(String customerId, String accountType, String type,String uniqueIdentifier)
			throws Exception {
		String sessionID = SessionIDGenerator.sessionId.toString().trim().replace("-", "");
		Response icsApiResponse = null;
		Response resp = getAccessTokenICS(customerId);
		/*Response fansResponse = fansAPiResponse(sessionID, customerId, accountType, type);
		System.out.println("Fansresponse" + fansResponse);
		String uniqueId = "00002609555298452488";*/
		// fansResponse.getBody().jsonPath().get(("tokens[0].uniqueIdentifier")).toString();
		String accessToken = resp.body().jsonPath().get("access_token");
		// JSONObject icsRequest =
		// getConvenienceChecksRequest(TestConstants.VALID_ICS_REQUEST,
		// TestConstants.API_REQUEST);

		JSONObject icsRequest = getConvenienceChecksRequest(TestConstants.VALID_ICS_REQUEST, TestConstants.API_REQUEST);
		APIDetails apiDetails = getAPIDetails("icsAPI");
		System.out.println("--> " + apiDetails);
		Headers headers = getAPIHeaders(apiDetails, sessionID);
		String apiURL = apiDetails.getUrl().replaceAll("ENV", env) + apiDetails.getParam();
		icsApiResponse = RestAssured.given()
				.config(RestAssured.config().encoderConfig(
						EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))
				.headers(headers).header("Authorization", "Bearer " + accessToken)
				.body(icsRequest.toString().replaceAll("UNIQUEXID", uniqueIdentifier)).when().post(apiURL).then().log().all()
				.extract().response();
		System.out.println("icsApiResponse--> " + icsRequest.toString().replaceAll("UNIQUEXID", uniqueIdentifier) + " ");
		System.out.println("icsApiResponse--> " + headers + " ");
		System.out.println("icsApiResponse--> " + apiURL + " ");
		// System.exit(0);
		return icsApiResponse;
	}

	/**
	 *
	 * @param userName
	 * @return the bearer token from ICS
	 * @throws IOException
	 */
	public static Response getAccessTokenICS(String userName) throws IOException {

		String sessionId = SessionIDGenerator.sessionId.toString().trim().replace("-", "");
		JSONObject request = getConvenienceChecksRequest(TestConstants.VALID_ICS_ACCESS_TOKEN_REQUEST,
				TestConstants.API_REQUEST);

		Map<String, String> parametersMap = getFormParam(request, sessionId, userName);

		APIDetails apiDetails = getAPIDetails("icsAccessToken");
		Headers headers = getAPIHeaders(apiDetails, sessionId);
		String apiURL = apiDetails.getUrl().replaceAll("ENV", env) + apiDetails.getParam();
		io.restassured.response.Response resp = RestAssured.given().log().all()
				.config(RestAssured.config().encoderConfig(
						EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))
				.headers(headers).formParams(parametersMap).when().post(apiURL).then().log().all().extract().response();
		return resp;
	}

	public static Map<String, String> getFormParam(JSONObject request, String sessionId, String userName) {

		Map<String, String> parametersMap = new HashMap<>();
		for (int i = 0; i < request.names().length(); i++) {
			try {
				parametersMap.put(request.names().getString(i), request.get(request.names().getString(i)).toString()
						.replaceAll("USERXNAME", userName).replaceAll("SESSIONXID", sessionId));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return parametersMap;
	}

	/**
	 * used to populate the session info from FAN's api
	 *
	 * @param sessionId
	 * @param userName
	 * @throws JSONException
	 */
	public static Response fansAPiResponse(String sessionId, String userName, String uniqueIdentifer)
			throws JSONException {

		Response fansAccountTokenResponse = null;

		try {
			Response apigeeResponse = RestAssuredHttpClient.fetchApigeeApiToken(userName, sessionId);
            String apigeeToken = apigeeResponse.getBody().jsonPath().get(("accessToken")).toString();
			System.out.println("************apigeeToken:" + apigeeToken);
            Response accountsListResponse = RestAssuredHttpClient.fetchAccountsListResponse(userName, sessionId, apigeeToken);
            System.out.println("************accountsListResponse:" + accountsListResponse);

			fansAccountTokenResponse = RestAssuredHttpClient.fetchFANSAccountTokenResponse(userName, sessionId, uniqueIdentifer);
			System.out.println("************fansAccountTokenResponse:" + fansAccountTokenResponse);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return fansAccountTokenResponse;
	}

	/**
	 * Compares both Domain API and ICS responses
	 *
	 //	 * @param rewards
	 //	 * @param response
	 * @return true if both the responses are same or else false
	 */
	public static boolean compareSourceAndTargetResponse(RewardDetailsICSResponse  rewards,
														 ConvenienceChecksDomainResponse response) {

		boolean result = false;
		if (rewards.getRewards().equals(response.getDetails())) {
			result = true;
			System.out.println("*****SUCCESS***** BOTH THE DOMAIN AND ICS RESPONSES ARE SAME");
		} else {
			result = false;
			System.out.println("!!!!FAILED!!!! \n BOTH THE DOMAIN AND ICS RESPONSES ARE NOT SAME");
		}
		return result;
	}

	public static String getUniqueIdentifer(JSONObject request) throws JSONException {

		String uniqueIdentifer = "";
		JSONArray accountType = request.getJSONObject("variables").getJSONObject("request")
				.getJSONArray("accountIdentifiers");
		int length = accountType.length();
		for (int i = 0; i < length; i++) {
			JSONObject jObj = accountType.getJSONObject(i);
			uniqueIdentifer = jObj.getString("uniqueIdentifier");
		}

		return uniqueIdentifer;
	}

	public static String getAccountToken(JSONObject request) throws JSONException {

		String uniqueIdentifer = "";
		uniqueIdentifer = request.getJSONObject("variables").getJSONObject("request").get("accountTokens").toString();

		return uniqueIdentifer;
	}

	/**
	 *
	 * @param fileName
	 * @return the json request object which has the account token
	 */
	public static org.json.JSONObject getAccountTokenRequest(String fileName, String folder) {
		org.json.JSONObject request = new org.json.JSONObject();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(folder + fileName));
			request = new org.json.JSONObject(obj.toString());
		} catch (IOException | org.json.simple.parser.ParseException | JSONException e) {
			e.printStackTrace();
		}
		return request;
	}

	public static void clearFiles() {

		String path = "src/test/resources/requests/NegativeScenarios/";
		File file = new File(path);
		if (file.exists()) {
			Arrays.stream(new File(path).listFiles()).forEach(File::delete);
			System.out.println(new File(path).listFiles());
		} else {
			file.mkdirs();
		}

	}
	public static void clearFiles1() {
		String path = "src/test/resources/requests/NegativeScenarios/";
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}

	}

	public static org.json.JSONObject readJsonFile(String fileName, String folder) {
		org.json.JSONObject request = new org.json.JSONObject();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(TestConstants.REQUESTS_FOLDER_NAME + folder + fileName));
			request = new org.json.JSONObject(obj.toString());
		} catch (IOException | org.json.simple.parser.ParseException | JSONException e) {
			e.printStackTrace();
		}
		return request;
	}
}
