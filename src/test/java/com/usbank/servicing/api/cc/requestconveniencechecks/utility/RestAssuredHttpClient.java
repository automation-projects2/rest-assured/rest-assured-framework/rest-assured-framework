package com.usbank.servicing.api.cc.requestconveniencechecks.utility;

import com.google.gson.Gson;
import com.omnimobile.DataObjects.PropertyConfigs;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.APIDetails;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.APIDetailsList;
import com.usbank.servicing.api.cc.requestconveniencechecks.model.APIHeaders;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RestAssuredHttpClient {
    private static PropertyConfigs propertyConfigs = new PropertyConfigs();
    public static String environment = propertyConfigs.urlProperty.getProperty("environment");
    public static String apigeeEnvironment = propertyConfigs.urlProperty.getProperty("apigeeEnvironment");
    public static String apigeeEnvironmentRoutingKey = propertyConfigs.urlProperty.getProperty("apigeeEnvironmentRoutingKey");
    public static String fansApiEnvironment = propertyConfigs.urlProperty.getProperty("fansApiEnvironment");

    public static Response fetchApigeeApiToken(String userName, String sessionId) throws Exception {
        APIDetails apiDetails = getAPIDetails("apigeeTokenApi");
        String apiURL = replaceVariable(apiDetails.getUrl(), "ENV", apigeeEnvironment) + apiDetails.getParam();
        JSONObject apigeeTokenApiRequestJson = readJsonFile(TestConstants.API_REQUEST, "apigee.json");
        JSONObject clientDataJson = readJsonFile(TestConstants.API_REQUEST, "clientDataHeader.json");
        String grantType = apigeeTokenApiRequestJson.get("grant_type").toString();
        String customData = replaceVariable(clientDataJson.get("clientData").toString(),
                "SESSIONXID", sessionId);
        customData = replaceVariable(customData, "USERXNAME", userName);
        List<Header> headersList = getAPIHeaders(apiDetails, sessionId);
        Headers headers = new Headers(headersList);
        String body = "grant_type" + "=" + grantType + "&" + "customData=" + customData;
        Response response = connect(apiURL, headers, body);
        return response;
    }

    public static Response fetchAccountsListResponse(String userName, String sessionId, String apigeeToken) throws Exception {
        APIDetails apiDetails = getAPIDetails("accountListApi");
        String apiURL = replaceVariable(apiDetails.getUrl(), "ENV", apigeeEnvironment) + apiDetails.getParam();
        JSONObject accountsListApiRequestJson = readJsonFile(TestConstants.API_REQUEST, "accountsList.json");

        String body = replaceVariable(accountsListApiRequestJson.get("query").toString(),
                "USERXNAME",
                userName);

        List<Header> headersList = getAPIHeaders(apiDetails, sessionId);
        headersList.add(new Header("Authorization", "Bearer " + apigeeToken));
        headersList.add(new Header("routingKey", apigeeEnvironmentRoutingKey));
        Headers headers = new Headers(headersList);
        Response response = connect(apiURL, headers, accountsListApiRequestJson.toString());
        return response;
    }

    public static Response fetchFANSAccountNumberResponse() throws Exception {
        return null;
    }

    public static Response fetchFANSAccountTokenResponse(String userName, String sessionId, String uniqueIdentifer) throws Exception {
        APIDetails apiDetails = getAPIDetails("fansAPI");
        String apiURL = replaceVariable(apiDetails.getUrl(),"ENV", fansApiEnvironment) + apiDetails.getParam();
        JSONObject fansApiRequestJson = readJsonFile(TestConstants.API_REQUEST, TestConstants.FANS_API_HEADER);
        JSONObject clientDataJson = readJsonFile(TestConstants.API_REQUEST, "clientDataHeader.json");
        String body = replaceVariable(fansApiRequestJson.get("body").toString(),"UNIQUEXID", uniqueIdentifer);

        System.out.println("****body:" + body);

        List<Header> headersList = getAPIHeaders(apiDetails, sessionId);

        String clientData = replaceVariable(clientDataJson.get("clientData").toString(),
                "SESSIONXID", sessionId);
        clientData = replaceVariable(clientData, "USERXNAME", userName);

        headersList.add(new Header("clientData", clientData));
        System.out.println("*******clientData:" + clientData);
        Headers headers = new Headers(headersList);

        Response response = connect(apiURL, headers, body);
        return response;
    }

    public static Response fetchConvenienceChecksResponse(String userName, String sessionId, String body) throws Exception {
        APIDetails apiDetails = getAPIDetails("ConvenienceChecksDomainAPI");
        String apiURL = replaceVariable(apiDetails.getUrl(),"ENV", environment) + apiDetails.getParam();

        List<Header> headersList = getAPIHeaders(apiDetails, sessionId);

        JSONObject clientDataJson = readJsonFile(TestConstants.API_REQUEST, "clientDataHeader.json");

        String clientData = replaceVariable(clientDataJson.get("clientData").toString(),
                "SESSIONXID", sessionId);
        clientData = replaceVariable(clientData, "USERXNAME", userName);

        headersList.add(new Header("clientData", clientData));
        System.out.println("*******clientData:" + clientData);

        Headers headers = new Headers(headersList);
        Response response = connect(apiURL, headers, body);
        return response;
    }

    public static Response connect(String apiURL, Headers headers, String body) throws Exception {
        System.out.println("**** Connecting to:" + apiURL);
        for(Header header: headers) {
            System.out.println(header.getName() + "::" + header.getValue());
        }

        System.out.println("Request Body ::" + body);

        io.restassured.response.Response response = RestAssured.given()
                .config(RestAssured.config().encoderConfig(
                        EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))
                .headers(headers)
                .body(body).when().post(apiURL).then().log().all().extract().response();
        return response;
    }

    public static List<Header> getAPIHeaders(APIDetails apiDetails, String sessionId) {
        List<APIHeaders> apiHeaders = apiDetails.getApiHeaders();
        List<Header> headerList = new ArrayList<>();
        apiHeaders.stream().forEach(a -> headerList
                .add(new Header(a.getHeaderName(), replaceVariable(a.getHeaderValue(),"SESSIONXID", sessionId))));
        return headerList;
    }

    public static APIDetails getAPIDetails(String apiType) {
        JSONObject jsonResponse = readJsonFile(TestConstants.API_DETAILS);
        Gson gson = new Gson();
        String jsonUserIdentifier = "";
        try {
            jsonUserIdentifier = jsonResponse.getString("apiDetailsList");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIDetailsList apiDetails = gson.fromJson(jsonUserIdentifier, APIDetailsList.class);

        return apiDetails.getApiDetails().stream().filter(a -> a.getApiType().equals(apiType)).findAny().get();
    }

    public static org.json.JSONObject readJsonFile(String folder, String fileName) {
        org.json.JSONObject request = new org.json.JSONObject();
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(TestConstants.REQUESTS_FOLDER_NAME + folder + fileName));
            request = new org.json.JSONObject(obj.toString());
        } catch (IOException | org.json.simple.parser.ParseException | JSONException e) {
            e.printStackTrace();
        }
        return request;
    }

    public static org.json.JSONObject readJsonFile(String filePath) {
        org.json.JSONObject request = new org.json.JSONObject();
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(filePath));
            request = new org.json.JSONObject(obj.toString());
        } catch (IOException | org.json.simple.parser.ParseException | JSONException e) {
            e.printStackTrace();
        }
        return request;
    }

    public static String replaceVariable(String s, String variableName, String variableValue) {
        String result = s;
        if(result != null) {
            result = result.replaceAll(variableName, variableValue);
        }
        return result;
    }
}
