package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import lombok.Data;

@Data
public class ConvenienceChecksEligibilityDetails {

	private Boolean eligibleForConvenienceChecks;
	private String reasonCode;

}