package com.usbank.servicing.api.cc.requestconveniencechecks.graphql;

import com.jira.automation.listener.Adaptivister;
import com.omnimobile.common.GraphQlApiBase;
import com.omnimobile.core.SessionIDGenerator;
import com.usbank.servicing.api.cc.requestconveniencechecks.utility.RestAssuredHttpClient;
import com.usbank.servicing.api.cc.requestconveniencechecks.utility.TestConstants;
import com.usbank.servicing.api.cc.requestconveniencechecks.utility.TestUtility;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

import static com.usbank.servicing.api.cc.requestconveniencechecks.utility.TestUtility.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Listeners(Adaptivister.class)
public class GetRequestConvenienceChecksAPIPositiveGraphql extends GraphQlApiBase {

	JSONObject customerDetails;
	private static final Logger logger = LogManager.getLogger(GetRequestConvenienceChecksAPIPositiveGraphql.class);

	@BeforeClass(alwaysRun = true)
	public void initializeTest() throws IOException {
		try {
			Properties prop = new Properties();
			String jiraIssue;
			String jiraFolderPath;
			String adaptivistFolderPath = "src/test/resources/Properties/adaptivist.properties";
			prop.load(new FileReader(adaptivistFolderPath));
			String subStringValue = prop.getProperty("issue");
			subStringValue = subStringValue.substring(8);
			jiraIssue = subStringValue;
			jiraFolderPath = prop.getProperty("folder");
			Adaptivister.setFolder(jiraFolderPath, GetRequestConvenienceChecksAPIPositiveGraphql.class);
			Adaptivister.setStoryID(jiraIssue);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@BeforeMethod(alwaysRun = true)
	public void initializeScoreChangeValue(Method method) throws Exception {
		customerDetails = getCustomerIdList();
	}

	@Test(testName = "Validate request convenience checks for valid user - Unique Identiferflow", groups = { "regressionTest",
			"smokeTest" }, description = "Validate Convenience Checks Eligibility of CCD user - UniqueIdentifer flow", enabled = true)

	public void validateConvenienceChecksDomainResponse() throws Exception {

		JSONObject validUniqueIdentifierTestsJson = TestUtility.readJsonFile("validUniqueIdentifierTests.json",
				TestConstants.POSITIVE_REQUESTS_FOLDER_NAME);

		String testName = validUniqueIdentifierTestsJson.get("testName").toString();
		System.out.println("validateConvenienceChecksDomainResponse():" + testName);
		JSONArray testData = validUniqueIdentifierTestsJson.getJSONArray("testData");

		for(int i=0; i< testData.length(); i++) {
			JSONObject testDataObj = (JSONObject) testData.get(i);
			String customerId = testDataObj.getString("userName");
			JSONObject requestObj = testDataObj.getJSONObject("request");
			JSONObject expectedResponseObj = testDataObj.getJSONObject("expectedResponse");
			logger.info("***************CUSTOMER ID******************" + customerId);

			String sessionID = SessionIDGenerator.sessionId.toString().trim().replace("-", "");
			Response apigeeResponse = RestAssuredHttpClient.fetchApigeeApiToken(customerId, sessionID);
			String apigeeToken = apigeeResponse.getBody().jsonPath().get(("accessToken")).toString();
			System.out.println("************apigeeToken:" + apigeeToken);

			Response accountsListResponse = RestAssuredHttpClient.fetchAccountsListResponse(customerId, sessionID, apigeeToken);
			System.out.println("************accountsListResponse:" + accountsListResponse);

			buildUniqueIdentifierRequestAndValidateResponse(customerId, sessionID, apigeeToken, requestObj, expectedResponseObj);
		}
	}

	@Test(testName = "2-Validate request convenience checks for valid user - Account tokenflow IT", groups = { "regressionTest",
			"smokeTest"}, description = "Validate Convenience Checks Eligibility of CCD user - Accounttoken flow", enabled = true)
	public void validateConvenienceChecksDomainWithIcsAccountToken() throws Exception {

		JSONObject validAccountTokenTestsJson = TestUtility.readJsonFile("validAccountTokenTests.json",
				TestConstants.POSITIVE_REQUESTS_FOLDER_NAME);

		String testName = validAccountTokenTestsJson.get("testName").toString();
		System.out.println("validateConvenienceChecksDomainWithIcsAccountToken():" + testName);
		JSONArray testData = validAccountTokenTestsJson.getJSONArray("testData");

		for(int i=0; i< testData.length(); i++) {
			JSONObject testDataObj = (JSONObject) testData.get(i);
			String customerId = testDataObj.getString("userName");
			JSONObject requestObj = testDataObj.getJSONObject("request");
			JSONObject expectedResponseObj = testDataObj.getJSONObject("expectedResponse");
			System.out.println("***************CUSTOMER ID******************" + customerId);

			String sessionID = SessionIDGenerator.sessionId.toString().trim().replace("-", "");
			Response apigeeResponse = RestAssuredHttpClient.fetchApigeeApiToken(customerId, sessionID);
			String apigeeToken = apigeeResponse.getBody().jsonPath().get(("accessToken")).toString();
			System.out.println("************apigeeToken:" + apigeeToken);

			Response accountsListResponse = RestAssuredHttpClient.fetchAccountsListResponse(customerId, sessionID, apigeeToken);
			System.out.println("************accountsListResponse:" + accountsListResponse);

			buildAccountTokenRequestAndValidateResponse(customerId, sessionID, apigeeToken, requestObj, expectedResponseObj);
		}
	}

	private void buildUniqueIdentifierRequestAndValidateResponse(String customerId,
																 String sessionID,
																 String apigeeToken,
																 JSONObject requestObj,
																 JSONObject expectedResponseObj) throws Exception {

		JSONObject request = getConvenienceChecksRequest(TestConstants.VALID_GRAPHQL_UI_QUERY,
				TestConstants.POSITIVE_REQUESTS_FOLDER_NAME);

		JSONObject variablesJSONObject = request.getJSONObject("variables");
		variablesJSONObject.put("request", requestObj);

		Response domainResponse = generateConvenienceChecksApiResponseGraphql(customerId, sessionID, request.toString());

		String uniqueIdentifier = requestObj.getJSONArray("accountIdentifiers").getJSONObject(0).getString("uniqueIdentifier");

		validateDomainApiResponse(domainResponse, 200, uniqueIdentifier,
				null, true);
	}

	private void buildAccountTokenRequestAndValidateResponse(String customerId,
															 String sessionID,
															 String apigeeToken,
															 JSONObject requestObj,
															 JSONObject expectedResponseObj) throws Exception {

		//String accountToken = TestUtility.fetchAccountsTokenUsingFANSAPI(customerId, uniqueIdentifier);

		JSONObject request = getConvenienceChecksRequest(TestConstants.VALID_GRAPHQL_AT_QUERY,
				TestConstants.POSITIVE_REQUESTS_FOLDER_NAME);
		JSONObject variablesJSONObject = request.getJSONObject("variables");
		variablesJSONObject.put("request", requestObj);

		//String requestBody = requestObj.toString().replace("ACCOUNTXTOKEN", accountToken);

		Response domainResponse = generateConvenienceChecksApiResponseGraphql(customerId, sessionID, request.toString());

		String accountToken = requestObj.getJSONArray("accountTokens").getString(0);
		String uniqueIdentifier = expectedResponseObj.getJSONArray("convenienceChecksEligibility").getJSONObject(0).getString("uniqueIdentifier");

		validateDomainApiResponse(domainResponse, 200, uniqueIdentifier,
				accountToken, true);
	}

	/**
	 * Validates whether the comparison of responses from Domain API and ICS are
	 * same or not
	 * 
	 * @param result
	 */
	private void validateComparisonResult(boolean result) {
		assertThat(result, equalTo(true));
	}

	private void validateDomainApiResponse(Response apiResponse, int expectedStatusCode,
										   String expectedUniqueIdentifier,
										   String expectedAccountToken,
										   boolean expectedEligibleForConvenienceChecks) throws JSONException {
		ValidatableResponse response = apiResponse.then().assertThat();
		response.statusCode(expectedStatusCode);

		//String bodyAsString = apiResponse.body().asString();

		response.body("data.convenienceChecksEligibility[0]", Matchers.anyOf(Matchers.notNullValue()));

		Assert.assertEquals(apiResponse.getBody().jsonPath().get(("data.convenienceChecksEligibility[0].accountToken")),
				expectedAccountToken,
				"Response body does not contains correct Token value: ");

		Assert.assertEquals(apiResponse.getBody().jsonPath().get(("data.convenienceChecksEligibility[0].uniqueIdentifier")),
				expectedUniqueIdentifier,
				"Response body does not contains correct uniqueIdentifier value: ");

		response.body("data.convenienceChecksEligibility[0].details[0]", Matchers.anyOf(Matchers.notNullValue()));

		Assert.assertEquals(apiResponse.getBody().jsonPath().get(("data.convenienceChecksEligibility[0].details[0].eligibleForConvenienceChecks")),
				new Boolean(expectedEligibleForConvenienceChecks),
				"Response body does not contains correct eligibleForConvenienceChecks value: ");
	}
}
