package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import lombok.Data;

@Data
public class TestData {

	private String testName;
	private String accountNumber;
	private String productCode;
	private String companyID;
	private String uniqueIdentifier;
	private String identifierType;

}
