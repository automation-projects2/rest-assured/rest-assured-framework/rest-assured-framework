package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@lombok.Data
public class DomainResponse {

	@JsonProperty("errors")
	private List<Errors> errors;

	@JsonProperty("data")
	private DomainData data;
}
