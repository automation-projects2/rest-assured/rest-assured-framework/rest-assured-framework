package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@lombok.Data
public class DomainData<T> {

	@JsonProperty("ConvenienceChecksResponse")
	private List<T> t;

}
