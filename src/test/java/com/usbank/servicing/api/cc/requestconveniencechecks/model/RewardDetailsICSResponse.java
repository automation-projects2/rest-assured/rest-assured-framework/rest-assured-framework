package com.usbank.servicing.api.cc.requestconveniencechecks.model;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Validated
public class RewardDetailsICSResponse {
    private String accountToken;

    private String uniqueIdentifier;

    @JsonProperty("data")
    private List<ConvenienceChecksEligibilityDetails> rewards;

    @JsonProperty("error")
    private PartialError error;
}
