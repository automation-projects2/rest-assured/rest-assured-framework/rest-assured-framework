package com.omnimobile.DataObjects;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.omnimobile.DataObjects.ApiObjects;

public class ApiLibrary {
	
	public ApiObjects GetApis() throws Exception {
		return new ObjectMapper().readValue(filePath(), ApiObjects.class);
	}

	private File filePath() {
		String filePath = System.getProperty("user.dir") + "/src/test/resources/ApiLibrary/ApiDetails.json";
		return new File(filePath);
	}

}


































/*package com.omnimobile.DataObjects;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ApiLibrary implements ApiDetailsInterface {
	

	
	String teamJson = null;	
	protected String filepath1;
	String file;
	
	public ApiObjects(String scenarios) {
		// TODO Auto-generated constructor stub
		setCredentials(scenarios);
	}


	//@SuppressWarnings("unused")
	public ApiObjects getApis(String scenarios) {
		ApiObjects ao = new ApiObjects();
		System.out.println("test");
		String[] scenariCase = scenarios.replace("Test ", "").split(" for ");
		System.out.println("scenariCase0:"+scenariCase[0]);
		System.out.println("scenariCase1:"+scenariCase[1]);
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		try {

		ao =  objectMapper.readValue(getfilePath(scenariCase[0]),ApiObjects.class);
	
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return ao;
	}


	private File getfilePath(String team) throws IOException{
		
		String filePath=System.getProperty("user.dir")+"\\src\\test\\resources\\ApiLibrary\\";
		String teamJson = null;
		switch (team.toLowerCase()) {
		case "closecardapi":
			teamJson =  "CloseCardApi.json";
			break;
		case "resetpinapi":
			teamJson =  "ResetPinApi.json";
			break;
		case "hotcardapi":
			//SMW
			teamJson =  "HotCardApi.json";
			break;
		case "reopenclosecardreqobject":
			//SMW
			teamJson =  "ReOpenCloseCardReqObject.json";
			break;
		case "getcardlist":
			//SMW
			teamJson =  "GetCardList.json";
			break;
		default:
			teamJson = null;
			break;
		}
		return new File(filePath+teamJson);
		
	}
	
	
	
	}

	
		*/