package com.omnimobile.DataObjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.omnimobile.DataObjects.ApiDetails.Details;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiObjects {

	@Getter
	@Setter
	public Details convenienceChecks;

}
