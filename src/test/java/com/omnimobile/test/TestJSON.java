package com.omnimobile.test;

import com.usbank.servicing.api.cc.requestconveniencechecks.model.TestData;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestJSON {

	public static void main(String[] args) {

		String testname = "NullUniqueIdentifier";
		createQueryAPIJSON(testname);
		//createMutationAPIJSON(testname);

	}

	public static JSONObject createQueryAPIJSON(String testname) {
		JSONObject jsonObject = new JSONObject();
		String csvFile = "src/test/resources/requests/json/testData.csv";
		String queryFilename = "src/test/resources/requests/PositiveScenarios/query.txt";
		List<TestData> testData = readCsvFile(csvFile);
		jsonObject = getRequestFiltered(testData, testname, queryFilename);
		return jsonObject;
	}

	public static JSONObject createMutationAPIJSON(String testname) {
		JSONObject jsonObject = new JSONObject();
		String csvFile = "src/test/resources/requests/json/testData.csv";
		String queryFilename = "src/test/resources/requests/PositiveScenarios/mutation.txt";
		List<TestData> testData = readCsvFile(csvFile);
		jsonObject = getRequestFiltered(testData, testname, queryFilename);
		return jsonObject;
	}

	public static JSONObject getRequestFiltered(List<TestData> testData, String testname, String queryFilename) {

		JSONObject jsonObject = new JSONObject();
		List<TestData> filteredData = testData.stream().filter(t -> t.getTestName().equalsIgnoreCase(testname))
				.collect(Collectors.toList());

		for (TestData data : filteredData) {
			jsonObject = getRequestCreated(data, testname, queryFilename);
		}
		return jsonObject;
	}

	public static JSONObject getRequestCreated(TestData data, String testname, String queryFilename) {

		JSONObject jsonObject = new JSONObject();
		JSONObject jsonObject1 = new JSONObject();
		JSONObject jsonObject2 = new JSONObject();
		JSONObject jsonObject3 = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		String filename = testname + ".json";

		try {
			jsonObject.put("query", readFileAsString(queryFilename));
			if (data.getTestName().equalsIgnoreCase("TestName")) {
			} else if (data.getTestName().equalsIgnoreCase("NullAccountToken")) {
				jsonArray.put(JSONObject.NULL);
				jsonObject2.put("accountTokens", jsonArray);
				jsonObject2.put("identifierType", data.getIdentifierType());
				jsonObject1.put("request", jsonObject2);
				jsonObject.put("variables", jsonObject1);
			} else if (data.getTestName().equalsIgnoreCase("InvalidAccountToken")
					|| data.getTestName().equalsIgnoreCase("EmptyAccountToken")
					|| data.getTestName().equalsIgnoreCase("EmptyIdentifer")
					|| data.getTestName().equalsIgnoreCase("InvalidIdentifer")) {

				jsonObject2.put("identifierType", data.getIdentifierType());
				jsonArray.put(data.getAccountNumber());
				jsonObject2.put("accountTokens", jsonArray);
				jsonObject1.put("request", jsonObject2);
				jsonObject.put("variables", jsonObject1);
			} else if (data.getTestName().equalsIgnoreCase("NullProductCode")) {
				// jsonArray.put(JSONObject.NULL);
				jsonObject3.put("productCode", JSONObject.NULL);
				jsonObject3.put("uniqueIdentifier", data.getUniqueIdentifier());
				jsonArray.put(jsonObject3);
				jsonObject2.put("accountIdentifiers", jsonArray);
				jsonObject2.put("identifierType", data.getIdentifierType());
				jsonObject1.put("request", jsonObject2);
				jsonObject.put("variables", jsonObject1);

			} else if (data.getTestName().equalsIgnoreCase("InvalidProductCode")
					|| data.getTestName().equalsIgnoreCase("BlankProductCode")
					|| data.getTestName().equalsIgnoreCase("EmptyProductCode")) {

				jsonObject3.put("productCode", data.getProductCode());
				jsonObject3.put("uniqueIdentifier", data.getUniqueIdentifier());
				jsonArray.put(jsonObject3);
				jsonObject2.put("accountIdentifiers", jsonArray);
				jsonObject2.put("identifierType", data.getIdentifierType());
				jsonObject1.put("request", jsonObject2);
				jsonObject.put("variables", jsonObject1);
			} else if (data.getTestName().equalsIgnoreCase("NullIdentiferType")) {
				jsonObject3.put("productCode", data.getProductCode());
				jsonObject3.put("uniqueIdentifier", data.getUniqueIdentifier());
				jsonArray.put(jsonObject3);
				jsonObject2.put("accountIdentifiers", jsonArray);
				jsonObject2.put("identifierType", JSONObject.NULL);
				jsonObject1.put("request", jsonObject2);
				jsonObject.put("variables", jsonObject1);


			} else if (data.getTestName().equalsIgnoreCase("InvalidIdentiferType")
					|| data.getTestName().equalsIgnoreCase("BlankIdentiferType")) {
				jsonObject3.put("productCode", data.getProductCode());
				jsonObject3.put("uniqueIdentifier", data.getUniqueIdentifier());
				jsonArray.put(jsonObject3);
				jsonObject2.put("accountIdentifiers", jsonArray);
				jsonObject2.put("identifierType", data.getIdentifierType());
				jsonObject1.put("request", jsonObject2);
				jsonObject.put("variables", jsonObject1);


			}
			FileWriter file = new FileWriter("src/test/resources/requests/NegativeScenarios/" + filename);
			file.write(jsonObject.toString());
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonObject;
	}

	public static String readFileAsString(String fileName) {
		String data = "";
		try {
			data = new String(Files.readAllBytes(Paths.get(fileName)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data.replaceAll("\\r\\n", " ");
	}

	public static List<TestData> readCsvFile(String fileName) {
		List<TestData> testDatas = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				TestData data = new TestData();
				data.setTestName(values[0]);
				data.setAccountNumber(values[1]);
				data.setProductCode(values[2]);
				data.setCompanyID(values[3]);
				data.setUniqueIdentifier(values[4]);
				data.setIdentifierType(values[5]);
				testDatas.add(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return testDatas;
	}

}
